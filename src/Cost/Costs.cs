﻿using System;

namespace Cost
{
    public class Costs
    {

        private const double rate = 20.45;
        private string name { get; set; }
        private double transport { get; set; }
        private uint days { get; set; }

        public Costs()
        {
            name = "";
            transport = 0.0;
            days = 0;
        }
        public Costs(string name, double transport, uint days)
        {
            this.name = name;
            this.transport = transport;
            this.days = days;
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public double Transport
        {
            get { return transport; }
            set { transport = value; }
        }
        public uint Days
        {
            get { return days; }
            set { days = value; }
        }
        public void Show()
        {
            System.Console.WriteLine($"{"Rate",-10} = {rate}");
            System.Console.WriteLine($"{"Name",-10} = {name}");
            System.Console.WriteLine($"{"Transport",-10} = {transport}");
            System.Console.WriteLine($"{"Days",-10} = {days}");
            System.Console.WriteLine($"{"Total",-10} = {GetTotal()}");
        }
        public override string ToString()
        {
            return $"{rate};{name};{transport};{days};{GetTotal()}";
        }
        private double GetTotal()
        {
            return transport + rate * days;
        }
    }
}
