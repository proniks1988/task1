﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cost;

namespace Task1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            const uint n = 5;
            Costs[] peoples = new Costs[n];
            peoples[0] = new Costs("Петров П.И", 58.33, 5);
            peoples[1] = new Costs("Сидоров В.Г", 68.95, 7);
            peoples[3] = new Costs("Иванов Д.В", 34.12, 3);
            peoples[4] = new Costs();

            System.Console.WriteLine();
            foreach (var x in peoples)
            {
                try
                {
                    x.Show();
                    System.Console.WriteLine();
                }
                catch (System.NullReferenceException exception)
                {
                    System.Console.WriteLine("Под этим номеров участник отсутствует\r\n");
                }
            }

            peoples[n - 1].Transport = 77.7;

            uint sum = peoples[0].Days + peoples[1].Days;
            System.Console.WriteLine(
                $"Общая продолжительность командировок двух первых участников равна  {sum}  \r\n");


            foreach (var x in peoples)
            {
                System.Console.WriteLine(x);
            }
        }
    }
}
